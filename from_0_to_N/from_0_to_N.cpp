#include <iostream>
using namespace std;

void Odd_even(int N, int k) {

		for (int i = k; i < N + 1; i += 2) {
			cout << i<< " ";
		}
		cout << "\n";
		
}

int main()
{
	int N, k;
	cout << "Enter N: " << "\n";
	cin >> N;

	cout << "Enter 1 if odd, and 2 if even: " << "\n";
	cin >> k;

	Odd_even(N,k);
	return 0;
}

